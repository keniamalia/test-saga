<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WebsiteController@index');
Route::get('/logout', 'UserController@logout');
Route::get('/profile', 'AdminController@profile');

Route::get('/admin', 'ArticleController@index');
Route::get('/admin/sidebar', 'AdminController@sidebar');
Route::get('admin/article/create', 'ArticleController@dropdown');
Route::resource('/admin/user', 'UserController');
Route::resource('/admin/article', 'ArticleController');
Route::resource('/admin/category', 'CategoryController');
Route::resource('/web', 'WebsiteController');
Route::get('/web/{category_slug}/{article_slug}', 'WebsiteController@show'); 

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
