<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('category')->insert([
            'name' => "beauty",
            'slug' => "beauty",
            'created_at' => date('Y-m-d')
        ]);
        DB::table('category')->insert([
            'name' => "technology",
            'slug' => "technology",
            'created_at' => date('Y-m-d')
        ]);
    }
}
