<?php

use Illuminate\Database\Seeder;

class ArticleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('article')->insert([
            'category_id' => "a",
            'title' => "Bagaimana Menjadi Baik",
            'slug' => "bagaimana-menjadi-baik",
            'content' => "Artikel ini membahas bagaimana menjadi baik",
            'original_banner' => "img_banner/lala.jpg",
            'small_banner' => "img_banner/lala.jpg",
            'created_at' => date('Y-m-d')
        ]);
    }
}
