<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert([
            'id' => 1,
            'name' => "admin",
            'email' => "admin@gmail.com",
            'password' => Hash::make("Password1!"),
            'address' => "Malang",
            "phone" => "081333660509",
            'created_at' => date('Y-m-d')
        ]);
    }
}
