<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    //
    protected $table = "article";

    protected $primarykey = "id";

    protected $fillable =  [
        'id',
        'title',
        'slug',
        'content',
        'original_banner',
        'small_banner',
        'category_id'
    ];
}
