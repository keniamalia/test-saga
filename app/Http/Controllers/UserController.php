<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(){
        $allUser = User::get(['id','name','email', 'phone', 'address']);
        //echo $article['article_id'];
        
        //return view('admin/article/article');
        return view('admin/user/user', compact('allUser'));
    }
    public function store(Request $request){
        $txtPassword = "";

        $txtName = $request->input('user_name');
        $txtEmail = $request->input('user_email');
        $txtPhone = $request->input('user_phone');
        $txtAddress = $request->input('user_address');
        //$txtPassword = ;

        if($request->has('password')){
            $txtPassword = Hash::make($request->input('password'));
        }
        else{
            $txtPassword = Hash::make('Password1!');
        }

        User::create([
            'name' => $txtName,
            'email' => $txtEmail,
            'phone' => $txtPhone,
            'address' => $txtAddress,
            'password' => $txtPassword
        ]);

        return redirect('/admin/user/');

    }
    
    public function destroy($id){

        $user = User::where('id', $id)->first();
        $user->delete();

        return redirect('/admin/user/');
    }
    public function show($id){
        
        $user_detail = User::get(['id','name', 'email', 'phone' ,'address']);
        
        //return view('customer.index', compact('customer'));
        return view('/admin/user/update', compact('user_detail'));
    }
    public function create(){
        
        return view('admin/user/create');
    }

    public function changePass(Request $request){

        $Id = $request->input('user_id');
        $oldPass = $request->input('old_pass');
        $newPass = $request->input('new_pass');

        $data_user = User::where('id', $id)->get()->first();

        //return response()->json($data_user);
        if (Hash::check($oldPass, $data_user->password)) {
            echo "iya";
            // User::where('id', $Id)->update([
            //     'password' => Hash::make($newPass)
                
            // ]);
            // return redirect('/admin/user/');
        }
        else{
            echo "tidak";
            // echo "<script>alert('Invalid old password')</script>";

            // return view('/admin/user/userprofile/');
        }

        
    }

    public function update(Request $request){
        
        $Id = $request->input('user_id');

        if(is_null($request->input('flag'))){
            $txtName = $request->input('user_name');
            $txtEmail = $request->input('user_email');
            $txtPhone = $request->input('user_phone');
            $txtAddress = $request->input('user_address');
        
        
            User::where('id', $Id)->update([
                'name' => $txtName,
                'email' => $txtEmail,
                'phone' => $txtPhone,
                'address' => $txtAddress
            ]);

            return redirect('/admin/user/');
        }
        else{
            $oldPass = $request->input('old_pass');
            $newPass = $request->input('new_pass');

            $data_user = User::where('id', $Id)->get()->first();

            //return response()->json($data_user);
            if (Hash::check($oldPass, $data_user->password)) {
                
                User::where('id', $Id)->update([
                    'password' => Hash::make($newPass)
                    
                ]);
                $asset = asset('/profile');
                echo "<script>alert('Change password successfuly'); window.location.href = '". $asset . "'</script>";
                //return redirect('/admin/user/');
            }
            else{
                //echo "tidak";
                $asset = asset('/profile');
                echo "<script>alert('Invalid old password'); window.location.href = '". $asset . "'</script>";

                //return redirect('/profile');
            }
        }
        
    }
    public function profile($id){
        
        return view('/admin/user/userprofile/');
    }

    public function logout(){
        Auth::logout();

        return redirect('/login');
    }

    
}
