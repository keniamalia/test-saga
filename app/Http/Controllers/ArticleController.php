<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\Category;
use View;
use Validator;

class ArticleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    //
    public function index(){
        $article = Article::join('category', 'article.category_id', '=', 'category.id')
                          ->select('article.id AS article_id', 'category.name AS category_name', 'article.title AS article_title', 'article.slug AS article_slug', 'article.original_banner AS banner', 'article.small_banner AS article_bannerSmall', 'article.content AS article_content')
                          ->get();

        //return response()->json($article);
        
        //return view('admin/article/article');
        return view('admin/article/article', compact('article'));
    }
    public function show($slug){
        
        $detail_article = Article::join('category', 'article.category_id', '=', 'category.id')
                                 ->where('article.slug', $slug)
                                 ->select('article.id AS article_id', 'category.name AS category_name', 'article.title AS article_title', 'article.slug AS article_slug', 'article.original_banner AS article_bannerOri', 'article.small_banner AS article_bannerSmall', 'article.content AS article_content', 'category.id AS category_id')
                                 ->get()
                                 ->first();
        $dropdown_category = Category::get(['name', 'id']);
        //return response()->json($detail_article);
        //return View::make('admin/article/detail', compact('detail_article'))->nest('', compact('detail_article'));
        return view('admin/article/update', compact('detail_article', 'dropdown_category'));
    }

    public function destroy($slug)
    {
        $customer = Article::where('slug', $slug)->first();
        $customer->delete();

        return redirect('/admin/article/');
    }
    public function create(){
        
        $category = Category::get(['id','name','slug']);
        
        return view('admin/article/create', compact('category'));
    }
    public function store(Request $request){

        $validator = Validator::make($request->all(),[
            'article_title' => 'required|string|max:50',
            'article_slug' => 'required|string|max:50',
            'article_content' =>'required|string'
        ])->validate();

        $file = $request->file('file_upload');
        $title = $request->input('article_title');
        $slug = $request->input('article_slug');
        $content = $request->input('article_content');
        $category = $request->input('category');
        
        $tujuan_upload = 'img_banner';
        $filename = "banner-" . date('ymdhms') . "." . $file->getClientOriginalExtension();
        $file->move($tujuan_upload, $filename);

        Article::create([
            'title' => $title,
            'slug' => $slug,
            'content' => $content,
            'original_banner' => $tujuan_upload . "\\". $filename,
            'small_banner' => $tujuan_upload . "\\". $filename,
            'category_id' => $category
        ]);
        return redirect('/admin/article/');
    }
    public function update(Request $request){

        $validator = Validator::make($request->all(),[
            'article_title' => 'required|string|max:50',
            'article_slug' => 'required|string|max:50',
            'article_content' =>'required|string'
        ])->validate();

        $tujuan_upload ="";
        $filename ="";
        $file = $request->file('file_upload');
        $title = $request->input('article_title');
        $slug = $request->input('article_slug');
        $content = $request->input('article_content');
        $category = $request->input('category');
        $id = $request->input('article_id');
        $oldBanner = $request->input('old_banner');
        
        
        if(!is_null($file)){
            $tujuan_upload = 'img_banner';
            $filename = "banner-" . date('ymdhms') . "." . $file->getClientOriginalExtension();
            $file->move($tujuan_upload, $filename);

            $filename = $tujuan_upload . "\\". $filename;

            echo "masuk sini";
        }
        else{
            $filename = $oldBanner;
        }
        //return $filename;
        Article::where('id', $id)->update([
            'title' => $title,
            'slug' => $slug,
            'content' => $content,
            'original_banner' => $filename,
            'small_banner' => $filename,
            'category_id' => $category
        ]);
        return redirect('/admin/article/');
    }

}
