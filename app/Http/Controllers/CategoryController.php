<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use Validator;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    //index
    public function index(){
        
        $category = Category::get(['id', 'name','slug']);
        
        return view('admin/category/category', compact('category'));
    }
    public function show($slug){

        $detailcategory = Category::where('slug', $slug)->get(['id', 'name', 'slug'])->first();

        return view('admin/category/update', compact('detailcategory'));
    }
    public function create(){
        return view('admin/category/create');
    }
    public function store(Request $request){
        $validator = Validator::make($request->all(),[
            'category_name' => 'required|string|max:50',
            'category_slug' => 'required|string|max:50',
        ])->validate();

        $txtName = $request->input('category_name');
        $textSlug= $request->input('category_slug');

        Category::create([
            'name' => $txtName,
            'slug' => $textSlug
        ]);

        return redirect('admin/category/');
    }
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'category_name' => 'required|string|max:50',
            'category_slug' => 'required|string|max:50',
        ])->validate();
        
        $Id = $request->input('category_id');
        $txtName = $request->input('category_name');
        $textSlug = $request->input('category_slug');
        
        Category::where('id', $Id)->update([
            'name' => $txtName,
            'slug' => $textSlug
        ]);

        return redirect('/admin/category/');
    }
    public function destroy($slug)
    {
        $customer = Category::where('slug', $slug)->first();
        $customer->delete();

        return redirect('/admin/category/category');
    }
}
