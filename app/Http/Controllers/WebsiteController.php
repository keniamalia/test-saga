<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\Category;
class WebsiteController extends Controller
{
    //
    public function index(){
        $data = Article::join('category', 'category.id', '=', 'article.category_id')->latest('article.created_at')
                         ->take(5)
                         ->select('article.id AS article_id', 'category.name AS category_name', 'article.title AS article_title', 'article.slug AS article_slug', 'article.original_banner AS article_bannerOri', 'article.small_banner AS article_bannerSmall', 'article.content AS article_content', 'category.id AS category_id', 'category.slug AS category_slug')
                         ->get();

        $data_first = $data->take(1)->first();
        
        $all_data = $data->where('article_id', '!=', $data_first->article_id)->take(4);
        
        $dropdown_category = Category::get(['name', 'id', 'slug']);

        return view('/website/news', compact('data_first', 'all_data', 'dropdown_category'));
    }
    public function show($category_slug, $article_slug = null){
        //echo $article_slug;
        $data = Article::join('category', 'category.id', '=', 'article.category_id')->latest('article.created_at')
                        ->where('category.slug', $category_slug)
                        ->select('article.id AS article_id', 'category.name AS category_name', 'article.title AS article_title', 'article.slug AS article_slug', 'article.original_banner AS article_bannerOri', 'article.small_banner AS article_bannerSmall', 'article.content AS article_content', 'category.id AS category_id', 'category.slug AS category_slug')
                        ->get();

        if(is_null($article_slug)){

            return view('/website/list', compact('data'));
        }
        else{
            
            $data = $data->where('article_slug', $article_slug)->first();

            return view('/website/detail', compact('data'));
        }
       
    }
    public function getArticleByCategory($category_id){

        $data = Article::join('category', 'category.id', '=', 'article.category_id')->latest('article.created_at')
                         ->where('article.category_id', $category_id)
                         ->select('article.id AS article_id', 'category.name AS category_name', 'article.title AS article_title', 'article.slug AS article_slug', 'article.original_banner AS article_bannerOri', 'article.small_banner AS article_bannerSmall', 'article.content AS article_content', 'category.id AS category_id')
                         ->get()
                         ->first();

        return view('/website/list', compact('data'));
    }
}
