<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;
use Auth;

class AdminController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(){
        return view('admin/article/article');
    }
    public function profile(){
        return view('admin/user/userprofile');
    }
    public function article(){
        return view('admin/article/article');
    }
}
