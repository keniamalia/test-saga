<!DOCTYPE html>
<html lang="en">

@include('admin/head');
<body class="">
  <div class="wrapper ">
    @include('admin/sidebar')
    <div class="main-panel">
      <!-- Navbar -->
      @include('admin/header')
      <!-- End Navbar -->
      <div class="content">
        @yield('content')
      </div>
    </div>
  </div>
@include('admin/js')
</body>

</html>