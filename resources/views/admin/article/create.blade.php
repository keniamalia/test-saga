@extends('admin/admin');
@section('pagetitle')
 - Article
@endsection
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card card-user">
            <div class="card-header">
                <h5 class="card-title">
                    Create Article
                </h5>
            </div>
            <div class="card-body">
                <form method= "POST" action = "/admin/article/" enctype="multipart/form-data">
                <!-- {{ method_field('PUT') }} -->
                {{ csrf_field() }}
                
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Category Name</label>
                                <select name="category" class="form-control">
                                <option value ="">Select Option</option>
                                @foreach($category as $item)
                                    <option value ="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                            <label>Title</label>
                            <input type="text" class="form-control" placeholder="Title" name="article_title" id="title" onkeyup="getSlug()" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                            <label>Slug</label>
                            <input type="text" class="form-control" name="article_slug" id= "article_slug"  readonly>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                            <label>Content</label>
                            <textarea name="article_content" class="form-control" placeholder="Type Content Here" required></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                            <label>Banner</label>
                                <div class="input-group mb-3">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" name="file_upload">
                                        <label class="custom-file-label">Choose file</label>
                                    </div>
                                </div>  
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="update ml-auto mr-auto">
                            <button type="submit" class="btn btn-primary btn-round" name="sbm_save" id="sbm_save">
                            Create Article
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">  
    function getSlug(){  
        var title = document.getElementById("title").value;
        //console.log(title);
        title = title.split(' ').join('-');

        var asd = document.getElementById("article_slug").value = title.toLowerCase();
    }  
</script>  
@endsection