@extends('admin/admin');
@section('pagetitle')
 - Article
@endsection
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card card-user">
            <div class="card-header">
                <h5 class="card-title">
                    Edit Article
                </h5>
            </div>
            <div class="card-body">
                <form method= "POST" action = "{{ url('admin/article/update/') }}" enctype="multipart/form-data" >
                {{ method_field('PUT') }}
                {{ csrf_field() }}
                
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Category Name</label>
                                <select name="category" class="form-control">
                                <!-- <option value="{{$detail_article->category_id}}" type="selected">{{$detail_article->category_name}}</option> -->

                                    @foreach($dropdown_category as $item)
                                        @if(!is_null($detail_article->category_id))
                                            @if($detail_article->category_id == $item->id)
                                                <option value="{{$detail_article->category_id}}" selected="selected">{{$detail_article->category_name}}</option>
                                            @else{
                                                <option value="{{$item->id}}">{{$item->name}}</option>
                                            }
                                            @endif
                                        @else
                                            <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                            <label>Title</label>
                            <input type="text" class="form-control" placeholder="Title" id="title" name="article_title" value = "{{$detail_article->article_title}}" onkeyup="getSlug()" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                            <label>Slug</label>
                            <input type="text" class="form-control" id="article_slug" name="article_slug" value = "{{$detail_article->article_slug}}" readonly required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                            <label>Content</label>
                            <textarea name="article_content" class="form-control" value="{{$detail_article->article_content}}" required>{{$detail_article->article_content}}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                            <label>Banner</label>
                            <input type="hidden" value = "{{$detail_article->article_bannerOri}}" name="old_banner">
                            <img src="{{asset($detail_article->article_bannerOri)}}" class="form-control">
                            <input type="file" class="custom-file-input" name="file_upload"><i style="color:red">Click here to upload your photo</i>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                            <input type="hidden" class="form-control" name="article_id" value="{{$detail_article->article_id}}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="update ml-auto mr-auto">
                            <button type="submit" class="btn btn-primary btn-round" >
                            Update Article
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    function getSlug(){  
        var title = document.getElementById("title").value;
        //console.log(title);
        title = title.split(' ').join('-');

        var asd = document.getElementById("article_slug").value = title.toLowerCase();
    }  
</script>
@endsection