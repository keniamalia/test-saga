@extends('admin/admin');
@section('pagetitle')
 - Articles
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="row" style="padding-left:930px">
      <a class="btn btn-primary btn-round" href="{{url('/admin/article/create/')}}">Add New</a></p>
    </div>
    <div class="card">
      <div class="card-body">
        <div class="table-responsive">
          <table class="table">
            <thead class=" text-primary">
              <th>Category</th>
              <th>Title</th>
              <th>Slug</th>
              <th>Content</th>
              <th>Banner</th>
              <th class="text-center">Action</th>
            </thead>
            <tbody>
              @foreach($article as $item)
              <tr>
                <td>{{$item->category_name}}</td>
                <td>{{$item->article_title}}</td>
                <td>{{$item->article_slug}}</td>
                <td width="330px">
                    <?php 
                      $num_char = 130;
                      $text = $item->article_content;
                      $text = substr($text, 0, $num_char) . ".....";
                    ?>
                    {{$text}}
                </td>
                <td><img src ="{{asset($item->banner)}}" width="60px"></td>
                <td class="text-center">
                  <form method="post" action="/admin/article/{{$item->article_slug}}">
                    <a class="btn btn-warning btn-round" href = "/admin/article/{{$item->article_slug}}">Edit</a>
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <button type="submit" class="btn btn-danger btn-round">Delete</button>
                  </form>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
</div>
@endsection