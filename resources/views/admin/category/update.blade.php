@extends('admin/admin');
@section('pagetitle')
 - Category
@endsection
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card card-user">
            <div class="card-header">
                <h5 class="card-title">
                    Edit Category
                </h5>
            </div>
            <div class="card-body">
                <form method= "POST" action = "{{ url('admin/category/update') }} " >
                {{ method_field('PUT') }}
                {{ csrf_field() }}
                
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                            <label>Category Name</label>
                            <input type="text" class="form-control" id="name" placeholder="Category Name" value = "{{ $detailcategory->name}}" name="category_name" onkeyup="getSlug()" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                            <label>Category Slug</label>
                            <input type="text" class="form-control" id="slug" placeholder="Category Slug" name="category_slug" value = "{{ $detailcategory->slug}}" readonly required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                            <input type="hidden" class="form-control" name="category_id" value="{{ $detailcategory->id}}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="update ml-auto mr-auto">
                            <button type="submit" class="btn btn-primary btn-round">Update Category</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">  
    function getSlug(){  
        var name = document.getElementById("name").value;
        //console.log(title);
        name = name.split(' ').join('-');

        var slug = document.getElementById("slug").value = name.toLowerCase();
    }  
</script>  
@endsection