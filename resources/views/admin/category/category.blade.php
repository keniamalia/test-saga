@extends('admin/admin');
@section('pagetitle')
 - Category
@endsection
@section('content')
<div class="row">
          <div class="col-md-12">
          <div class="row" style="padding-left:930px">
              <a class="btn btn-primary btn-round" href="{{url ('admin/category/create') }}">Add New</a></p>
          </div>
            <div class="card">
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table">
                    <thead class=" text-primary">
                      <th>Category Name</th>
                      <th>Slug</th>
                      <th class="text-center">Action</th>
                    </thead>
                    <tbody>
                    @foreach ($category as $item)
                      <tr>
                        <td>{{ $item->name }}</td>
                        <td>{{ $item->slug }}</td>
                        <td class="text-center">
                          <form method="post" action="/admin/category/{{$item->slug}}">
                            <a class="btn btn-warning btn-round" href = "/admin/category/{{$item->slug}}">Edit</a>
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button type="submit" class="btn btn-danger btn-round">Delete</button></td>
                          </form>
                        <td>
                        
                        
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
@endsection