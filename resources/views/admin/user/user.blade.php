@extends('admin/admin');
@section('pagetitle')
 - Users
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="row" style="padding-left:930px">
      <a class="btn btn-primary btn-round" href="{{ url('admin/user/create')}}">Add New</a></p>
    </div>
    <div class="card">
      <div class="card-body">
        <div class="table-responsive">
          <table class="table">
            <thead class=" text-primary">
              <th>Name</th>
              <th>Email</th>
              <th>Phone</th>
              <th>Address</th>
              <th class="text-center">Action</th>
            </thead>
            <tbody>
            @foreach($allUser as $item)
              <tr>
                <td>{{$item->name}}</td>
                <td>{{$item->email}}</td>
                <td>{{$item->phone}}</td>
                <td>{{$item->address}}</td>
                <td class="text-center">
                  <form method="post" action="/admin/user/{{$item->id}}">
                    <a class="btn btn-warning btn-round" href = "/admin/user/{{$item->id}}">Edit</a>
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <button type="submit" class="btn btn-danger btn-round">Delete</button>
                  </form>
                </td>
              </tr>
            @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
</div>
@endsection