@extends('admin/admin');
@section('pagetitle')
 - User
@endsection
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card card-user">
            <div class="card-header">
                <h5 class="card-title">
                    Create User
                </h5>
            </div>
            <div class="card-body">
                <form method="POST" action = "{{url('/admin/user/')}}" >
                <!-- {{ method_field('PUT') }}
                {{ csrf_field() }} -->
                {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Full Name</label>
                                <input type="text" class="form-control" placeholder="Full Name" name="user_name" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                            <label>Email</label>
                            <input type="email" class="form-control" placeholder="Email" name="user_email" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                            <label>Address</label>
                            <input type="text" class="form-control" name="user_address" placeholder="Address" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                            <label>Phone</label>
                            <input type="text" class="form-control" placeholder="Phone" name="user_phone" required> 
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="checkbox" id="random_password" onclick='hidePassword();'>&nbsp Default Password
                            </div>
                        </div>
                    </div>
                    <!-- ada kalau random password tidak di centang, hilang kalau di centang -->
                    <div id="pass">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                            <label id="lbl_password">Password</label>
                            <input type="password" class="form-control" id="password" placeholder="Password" name="user_password">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                            <label id="lbl_confirmPassword">Confirm Password</label>
                            <input type="password" class="form-control" id="confirm_password" placeholder="Confirm Password">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                            <input type="hidden" class="form-control" name="article_id" value="">
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="update ml-auto mr-auto">
                            <button type="submit" class="btn btn-primary btn-round" name="sbm_save" id="sbm_save"> 
                            Create User
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    function hidePassword(){
        var checkBox = document.getElementById("random_password");

            if(checkBox.checked == true){
                document.getElementById("lbl_password").style.display="none";
                document.getElementById("password").style.display="none";
                document.getElementById("lbl_confirmPassword").style.display="none";
                document.getElementById("confirm_password").style.display="none";
            }
            else{
                document.getElementById("lbl_password").style.display="block";
                document.getElementById("password").style.display="block";
                document.getElementById("lbl_confirmPassword").style.display="block";
                document.getElementById("confirm_password").style.display="block";
            }
            
        }
</script>
@endsection