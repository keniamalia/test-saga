@extends('admin/admin');
@section('pagetitle')
 - User
@endsection
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card card-user">
            <div class="card-header">
                <h5 class="card-title">
                    Edit
                </h5>
            </div>
            <div class="card-body">
                <form method= "POST" action = "{{ url('admin/user/update/') }}" >
                {{ method_field('PUT') }}
                {{ csrf_field() }}
                
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Full Name</label>
                                <input type="text" class="form-control" placeholder="Full Name" name="user_name" value = "{{$user_detail->first()->name}}" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                            <label>Email</label>
                            <input type="email" class="form-control" placeholder="Email" name="user_email" value = "{{$user_detail->first()->email}}" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                            <label>Address</label>
                            <input type="text" class="form-control" name="user_address" value = "{{$user_detail->first()->address}}" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                            <label>Phone</label>
                            <input type="text" class="form-control" placeholder="Title" name="user_phone" value = "{{$user_detail->first()->phone}}" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                            <input type="hidden" class="form-control" placeholder="" name="user_id" value = "{{$user_detail->first()->id}}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="update ml-auto mr-auto">
                            <button type="submit" class="btn btn-primary btn-round" name="sbm_save" id="sbm_save">
                            Update User
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection