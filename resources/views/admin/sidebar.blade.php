<div class="sidebar" data-color="white" data-active-color="danger">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
    -->
      <div class="logo">
        <a href="" class="simple-text logo-mini">
          <div class="logo-image-small">
            <img src="{{asset('paper/assets/img/logo-small.png')}}">
          </div>
        </a>
        <a href="/profile" class="simple-text logo-normal">{{$user->first()->name}}</a>
        <a href="/logout">Logout</a>
        
      </div>
      <div class="sidebar-wrapper">
        <ul class="nav">
          <li>
            <a href="{{url('admin/article')}}">
              <i class="nc-icon nc-paper"></i>
              <p>Article</p>
            </a>
          </li>
          <li>
            <a href="{{url('admin/category')}}">
              <i class="nc-icon nc-globe"></i>
              <p>Category</p>
            </a>
          </li>
          <li>
            <a href="{{url('admin/user')}}">
              <i class="nc-icon nc-circle-10"></i>
              <p>User</p>
            </a>
          </li>
        </ul>
      </div>
    </div>