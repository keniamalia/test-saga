@extends('website/main')
@section('content')
<div class="container">
<div class="row">
			
				<div class="col-md-12">
					
					<h4 class="p-title"><b>{{$data->first()->category_name}}</b></h4>
					<div class="row">
						@foreach($data as $item)
						<div class="col-sm-4">
							<img src="{{asset($item->article_bannerSmall)}}" alt="">
							<h4 class="pt-20"><a href="/web/{{$item->category_slug}}/{{$item->article_slug}}"><b>{{$item->article_title}}</b></a></h4>
							<hr>
							<p>
							<?php 
								$num_char = 100;
								$text = $item->article_content;
								$text = substr($text, 0, $num_char) . ".....";
							?>
							{{$text}}</p>
						</div><!-- col-sm-6 -->
						@endforeach
						
					</div><!-- row -->
					
					
				</div><!-- col-md-9 -->
				
				
				
            </div><!-- row -->
</div>
@endsection