@extends('website/main')

@section('content')
<div class="container">
			<div class="row">
			
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-12">
							<h4 class="p-title"><b>RECENT NEWS</b></h4>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<form action="/news" method="POST">
							{{ csrf_field() }}
							<div class="col-md-6">
									<label><b>Filter By Category</b></label>
									<select id="category"  class="form-control" onchange="getArticle()">
									<option value="">Select Option</a></option>
										@foreach($dropdown_category as $item)
											<option value="{{$item->slug}}">{{$item->name}}</a></option>
										@endforeach
									</select>	
							</div>
							</form>
						</div>
					</div>
					<br>
					<div class="row">
					
						<div class="col-sm-6">
							<img src="{{$data_first->article_bannerOri}}" alt="">
							<h4 class="pt-20"><a href="/web/{{$data_first->category_slug}}/{{$data_first->article_slug}}"><b>{{$data_first->article_title}}</b></a></h4>
							<ul class="list-li-mr-20 pt-10 pb-20">
								<li class="color-lite-black">Category: <a href="#" class="color-black"><b>{{$data_first->category_name}}</b></a></li>
							</ul>
							<p>{{$data_first->article_content}}</p>
						</div><!-- col-sm-6 -->
						
						<div class="col-sm-6">
						@foreach($all_data as $item)
							<a class="oflow-hidden pos-relative mb-20 dplay-block" href="/web/{{$item->category_slug}}/{{$item->article_slug}}">
								<div class="wh-100x abs-tlr"><img src="{{$item->article_bannerOri}}" alt=""></div>
								<div class="ml-120 min-h-100x">
									<h4><b>{{$item->article_title}}</b></h4>
									<h6><i>{{$item->category_name}}</i></h6>
									<hr>
									<p>
										<?php 
											$num_char = 100;
											$text = $item->article_content;
											$text = substr($text, 0, $num_char) . ".....";
										?>
										{{$text}}</p>
								</div>
							</a><!-- oflow-hidden -->
							@endforeach
						</div><!-- col-sm-6 -->
						
					</div><!-- row -->
					
					
					
			</div><!-- row -->
		</div><!-- container -->
		<script>
			function getArticle(){
				var e = document.getElementById("category");
				var strUser = e.options[e.selectedIndex].value;
				//console.log(strUser);

				var asset = "/web/"
				window.location.href = asset + strUser + "/";
			}
		</script>
@endsection