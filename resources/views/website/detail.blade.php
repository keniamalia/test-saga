@extends('website/main')
@section('content')
<section>
		<div class="container">
			<div class="row">
			
				<div class="col-md-12">
					<img src="{{asset($data->article_bannerOri)}}" alt="{{asset($data->article_bannerOri)}}">
					<h3 class="mt-30"><b>{{$data->article_title}}</b></h3>
					<p>{{$data->article_content}}</p>
					
					<div class="float-left-right text-center mt-40 mt-sm-20">
				
						<ul class="mb-30 list-li-mt-10 list-li-mr-5 list-a-plr-15 list-a-ptb-7 list-a-bg-grey list-a-br-2 list-a-hvr-primary ">
							<li><a href="#">{{$data->category_name}}</a></li>
						</ul>
						<ul class="mb-30  list-a-hw-radial-35 list-a-hvr-primary list-li-ml-5">
							<li><a href="/web">Back</a></li>
							
						</ul>
						
					</div><!-- float-left-right -->
				
					<div class="brdr-ash-1 opacty-5"></div>
					
					
			</div><!-- row -->
			
        </div><!-- container -->
</section>
@endsection