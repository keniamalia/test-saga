<!DOCTYPE HTML>
<html lang="en">
<head>
	<title>Saga-Test</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="UTF-8">
	
	<!-- Font -->
	<link href="https://fonts.googleapis.com/css?family=Encode+Sans+Expanded:400,600,700" rel="stylesheet">
	
	<!-- Stylesheets -->
	
	<link href="{{asset('newsbit/plugin-frameworks/bootstrap.css')}}" rel="stylesheet">
	
	<link href="{{asset('newsbit/fonts/ionicons.css')}}" rel="stylesheet">
	
		
	<link href="{{asset('newsbit/common/styles.css')}}" rel="stylesheet">
	
	
</head>
<body>
	@include('website/header')
	<div class="container">
		@yield('content')
	</div>
	<section>
	@yield('ulala')
</section>
	
	<!-- SCIPTS -->
	
		
	<script src="{{asset('newsbit/plugin-frameworks/jquery-3.2.1.min.js')}}"></script>
	
	<script src="{{asset('newsbit/plugin-frameworks/tether.min.js')}}"></script>
	
	<script src="{{asset('newsbit/plugin-frameworks/bootstrap.js')}}"></script>
	
	<script src="{{asset('newsbit/common/scripts.js')}}"></script>
</body>
</html>